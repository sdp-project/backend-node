// Dependencies
var mysql = require("mysql"),
  config = require("./dbConfig");

/*
 * @sqlConnection
 * Creates the connection, makes the query and close it to avoid concurrency conflicts.
 */
var sqlConnection = function sqlConnection(sql, values, next) {
  // It means that the values hasnt been passed
  if (arguments.length === 2) {
    next = values;
    values = null;
  }

  var connection = mysql.createConnection(config.db);
  connection.connect(function (err) {
    if (err !== null) {
      console.log("[MYSQL] Error connecting to mysql:" + err + "\n");
    }
  });
  const query = connection.query(sql, values, function (err) {
    connection.end(); // close the connection
    if (err) {
      console.log("SQL error",err);
      next(err);
      // throw err;
    }
    // Execute the callback
    else next.apply(this, arguments);
  });
  console.log('sql:',query.sql)
};

module.exports = sqlConnection;
