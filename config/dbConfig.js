const config = require('../env.'+(process.env.NODE_ENV || 'development')+'.json');
module.exports ={
  "db": {
    "host": config.DB_HOST,
    "user": config.DB_USER,
    "password": config.DB_PASSWORD,
    "database": config.DATABASE,
    "connectionLimit": 15,
    "queueLimit": 30,
    "acquireTimeout": 1000000,
    "multipleStatements": true
  }
}