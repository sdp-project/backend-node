-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2020 at 01:55 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e2`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addToCart` (OUT `OcartStatus` TEXT, IN `IuserID` INT, OUT `OcartID` INT, IN `IstockID` INT, IN `IvendorID` INT, IN `Iquantity` INT, OUT `OitemStatus` TEXT)  BEGIN
	SELECT id INTO OcartID FROM carts WHERE userID=IuserID;
    IF OcartID IS NULL THEN
        INSERT INTO carts SET userID = IuserID;
        SET OcartID = LAST_INSERT_ID();
        SET OcartStatus = 'new cart created.';
    ELSE
    	SET OcartStatus = 'cart exist';
    END IF;
        INSERT INTO cartitems SET cartID=OcartID, stockID=IstockID , vendorID=IvendorID , quantity=Iquantity;
		SET OitemStatus = "items added to cart";
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `placeOrder` (IN `IuserID` INT, IN `IcardID` INT, IN `IaddressID` INT, IN `Iuuid` VARCHAR(255), OUT `Oflag` INT, OUT `OorderID` INT, OUT `Omsg` VARCHAR(255), OUT `OcartCount` INT)  MODIFIES SQL DATA
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN
          SET Oflag = 0;
          SET Omsg = "order not placed. insufficient stock";
          ROLLBACK;
    END;
    START TRANSACTION;
        SET OcartCount = (SELECT COUNT(*) FROM `carts` WHERE carts.userID = IuserID);
        IF OcartCount > 0 THEN
            INSERT INTO orders SET userID = IuserID, cardID=IcardID, uuid = Iuuid, addressID = IaddressID;
            SET OorderID = LAST_INSERT_ID();
            INSERT INTO orderitems (orderID,stockID,vendorID,quantity,price) 
                SELECT OorderID,stockID,vendorID,quantity,price  
                FROM cartitems, carts 
                WHERE cartitems.cartID=carts.id AND carts.userID = IuserID;
            UPDATE orders SET orders.totalPrice = 
                (SELECT SUM(price)  
                FROM cartitems, carts 
                WHERE cartitems.cartID=carts.id AND carts.userID = IuserID)
            WHERE id = OorderID;
            DELETE FROM cartitems
                WHERE cartitems.cartID=(SELECT carts.id FROM carts WHERE carts.userID = IuserID);
            DELETE FROM carts WHERE carts.userID = IuserID;
            COMMIT;
            SET Oflag = 1;
            SET Omsg = "order placed";
        ELSE
        	SET Oflag = 0;
            SET Omsg = "No cart found";
        END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateAndViewCartPrice` (IN `IuserID` INT)  MODIFIES SQL DATA
BEGIN
    UPDATE cartitems,warehouse, carts
    SET cartitems.price = (warehouse.price - (warehouse.price*warehouse.discount/100))*cartitems.quantity    
    WHERE cartitems.stockID = warehouse.id AND cartitems.cartID = carts.id AND carts.userID = IuserID;
    SET @rownr=0;
    SELECT @rownr:=@rownr+1 AS rowID,CI.id, CI.stockID, P.productTitle, CI.quantity, CONCAT(CF.filterCriteria, '-', CF.filterType) AS Type, CI.price, CONCAT(U.fName,' ', U.lName) as vendor,  
        case 
            when CI.inStock != 0 
            then 'true'
            else 'false'
         END as inStock
    FROM cartitems AS CI, warehouse as WH, products as P, categoryfilter AS CF, users as U, carts  
    WHERE CI.stockID = WH.id AND WH.productID = P.id and WH.categoryFilterID = CF.id AND WH.vendorID = U.id AND CI.cartID = carts.id AND carts.userID = IuserID;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `viewProductAndVendors` (IN `IproductUUID` INT)  READS SQL DATA
BEGIN
	SELECT products.id,products.productTitle,products.MRP,products.productDesc,products.imageURL,products.categoryID,products.rating,products.qtySold,products.weight,products.GTIN,products.numberOfComment,products.uuid,products.brand FROM products WHERE products.uuid = IproductUUID;
    SELECT warehouse.vendorID,CONCAT( users.fName,' ',users.lName) as vendorName,warehouse.id as stockID, warehouse.stock, CONCAT(categoryfilter.filterCriteria,'-',categoryfilter.filterType) as varient, warehouse.price, warehouse.discount, (warehouse.price-(warehouse.discount/100)*warehouse.price) as discountPrice FROM warehouse,products,users,categoryfilter  WHERE warehouse.productID=products.id AND products.uuid = IproductUUID AND users.id=warehouse.vendorID AND products.categoryID = categoryfilter.categoryID AND warehouse.categoryFilterID = categoryfilter.id;
    
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `postalCode` int(11) NOT NULL,
  `country` varchar(20) NOT NULL,
  `userID` int(11) NOT NULL,
  `mobileNumber` int(11) NOT NULL,
  `type` enum('Home','Office') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `address1`, `address2`, `city`, `state`, `postalCode`, `country`, `userID`, `mobileNumber`, `type`) VALUES
(1, 'adr 1', 'addr 2', 'qwe', 'qweqwe', 21442, 'ads', 4, 214241, 'Office'),
(2, 'address1', 'address2', 'new', 'state', 234234, 'country', 3, 2147483647, 'Home'),
(3, 'new addr 3', 'address2', 'new', 'state', 234234, 'country', 4, 312312, 'Home'),
(4, 'new addr 3', '3rd addr', 'new', 'state', 234234, 'country', 3, 312312, 'Home'),
(5, 'addr for 4', 'addr', 'new', 'state', 234234, 'country', 4, 12312312, 'Office'),
(7, 'addr for 4', 'addrasda', 'new', 'state', 234234, 'country', 4, 12312312, 'Office');

-- --------------------------------------------------------

--
-- Table structure for table `carddetails`
--

CREATE TABLE `carddetails` (
  `id` int(11) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `cardNumber` varchar(20) NOT NULL,
  `type` enum('credit','debit') NOT NULL,
  `provider` enum('Visa','Mastercard','Amex') NOT NULL,
  `userID` int(11) NOT NULL,
  `expiryMM` int(11) NOT NULL,
  `expiryYY` int(11) NOT NULL,
  `nameOnCard` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carddetails`
--

INSERT INTO `carddetails` (`id`, `uuid`, `cardNumber`, `type`, `provider`, `userID`, `expiryMM`, `expiryYY`, `nameOnCard`) VALUES
(1, 'd4bd15a8-85bb-42e6-a1e6-31060b40fd91', '5425233430109903', 'credit', 'Mastercard', 3, 4, 23, 'asd asd'),
(2, '6224292f-b696-4c0a-ad06-22cdc4034d4f', '4263982640269299', 'credit', 'Visa', 4, 2, 23, 'asda asdas'),
(3, '3ad1d58b-de7f-450a-b7d6-1edc7718e1e5', '4263982640269299', 'credit', 'Visa', 4, 2, 23, 'asd asdas'),
(4, '38bc083d-c63a-42c8-97e3-412e476d792d', '4263982640269299', 'debit', 'Visa', 3, 2, 23, 'asd asdas'),
(6, 'ec135c96-0150-4302-b9f2-839740a493e3', '378282246310005', 'debit', 'Amex', 3, 6, 26, 'new name 6');

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `cartID` int(11) NOT NULL,
  `stockID` int(11) NOT NULL,
  `inStock` tinyint(1) NOT NULL DEFAULT 1,
  `vendorID` int(11) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` int(10) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`id`, `cartID`, `stockID`, `inStock`, `vendorID`, `quantity`, `price`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 1, 3, '1', 72, '2020-05-29 23:53:14', '2020-06-06 23:42:26'),
(2, 1, 1, 1, 3, '1', 72, '2020-05-29 23:55:05', '2020-06-06 23:42:26'),
(3, 1, 4, 1, 3, '3', 532, '2020-05-30 00:03:22', '2020-06-06 23:42:26'),
(20, 1, 4, 1, 3, '3', 532, '2020-06-04 20:24:37', '2020-06-06 23:42:26'),
(23, 1, 1, 1, 3, '12', 864, '2020-06-05 19:40:50', '2020-06-06 23:42:26'),
(26, 1, 6, 1, 5, '4', 318, '2020-06-07 02:09:13', '2020-06-06 23:42:26'),
(27, 1, 9, 1, 5, '15', 0, '2020-06-07 05:15:41', '2020-06-06 23:45:41');

--
-- Triggers `cartitems`
--
DELIMITER $$
CREATE TRIGGER `cartItemUpdated` BEFORE UPDATE ON `cartitems` FOR EACH ROW SET NEW.updatedAt = NOW()
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `dateCreated` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `userID`, `dateCreated`) VALUES
(1, 3, '2020-05-29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `catTitle` text NOT NULL,
  `catDesc` text NOT NULL,
  `imgUrl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `catTitle`, `catDesc`, `imgUrl`) VALUES
(1, 'fashion', 'cat 1 for the products', ''),
(2, 'cat 2', 'cat 2 for the products patch', '');

-- --------------------------------------------------------

--
-- Table structure for table `categoryfilter`
--

CREATE TABLE `categoryfilter` (
  `id` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `filterCriteria` varchar(20) NOT NULL,
  `filterType` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categoryfilter`
--

INSERT INTO `categoryfilter` (`id`, `categoryID`, `filterCriteria`, `filterType`) VALUES
(1, 1, 'size', 'L'),
(2, 1, 'size', 'M'),
(3, 1, 'size', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `stockID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `shippingDate` date DEFAULT NULL,
  `shippingStatus` enum('Preparing for Dispatch','Dispatched','On the way','Delivered') NOT NULL DEFAULT 'Preparing for Dispatch',
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT current_timestamp(),
  `vendorID` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `orderID`, `stockID`, `quantity`, `shippingDate`, `shippingStatus`, `createdAt`, `updatedAt`, `vendorID`, `price`) VALUES
(1, 2, 1, 5, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 360),
(2, 2, 1, 6, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 432),
(3, 2, 1, 7, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 504),
(4, 2, 1, 8, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 576),
(5, 2, 1, 9, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 648),
(6, 2, 1, 10, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 720),
(7, 2, 1, 11, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 792),
(8, 2, 1, 12, NULL, 'Preparing for Dispatch', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 3, 864),
(16, 3, 4, 2, NULL, 'Preparing for Dispatch', '2020-06-04 14:55:41', '2020-06-04 20:25:41', 3, 355),
(17, 3, 1, 3, NULL, 'Preparing for Dispatch', '2020-06-04 14:55:41', '2020-06-04 20:25:41', 3, 216),
(19, 4, 1, 12, NULL, 'Preparing for Dispatch', '2020-06-05 11:52:15', '2020-06-05 17:22:15', 3, 864),
(20, 6, 1, 12, NULL, 'Preparing for Dispatch', '2020-06-05 14:11:47', '2020-06-05 19:41:47', 3, 864);

--
-- Triggers `orderitems`
--
DELIMITER $$
CREATE TRIGGER `reduceStockForOrder` BEFORE INSERT ON `orderitems` FOR EACH ROW UPDATE warehouse
SET warehouse.stock = warehouse.stock - NEW.quantity
WHERE warehouse.id = NEW.stockID
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateProductsSold` AFTER INSERT ON `orderitems` FOR EACH ROW UPDATE products, warehouse
SET products.qtySold = products.qtySold + NEW.quantity
WHERE warehouse.id = NEW.stockID AND products.id = warehouse.productID
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `totalPrice` int(10) NOT NULL,
  `orderStatus` enum('inprogress','completed','','') NOT NULL DEFAULT 'inprogress',
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT current_timestamp(),
  `userID` int(11) NOT NULL,
  `addressID` int(11) NOT NULL,
  `error` varchar(255) NOT NULL,
  `paymentDate` datetime NOT NULL DEFAULT current_timestamp(),
  `transactionStatus` enum('pending','success','failed','') NOT NULL DEFAULT 'pending',
  `cardID` int(11) NOT NULL,
  `uuid` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `totalPrice`, `orderStatus`, `createdAt`, `updatedAt`, `userID`, `addressID`, `error`, `paymentDate`, `transactionStatus`, `cardID`, `uuid`) VALUES
(2, 4896, 'inprogress', '2020-06-03 17:13:02', '2020-06-03 22:43:02', 4, 3, '', '2020-06-03 22:43:02', '', 1, '29ade650-b8dc-413a-96a0-792564adf66b'),
(3, 571, 'inprogress', '2020-06-04 14:55:40', '2020-06-04 20:25:40', 4, 3, '', '2020-06-04 20:25:40', 'pending', 1, '8837a5eb-59e3-477b-ad6e-745d0673788f'),
(4, 864, 'inprogress', '2020-06-05 11:52:15', '2020-06-05 17:22:15', 4, 3, '', '2020-06-05 17:22:15', 'pending', 1, 'fadfa-23eda23'),
(5, 0, 'inprogress', '2020-06-05 12:10:04', '2020-06-05 17:40:04', 4, 3, '', '2020-06-05 17:40:04', 'pending', 1, 'fadfa-23eda23'),
(6, 864, 'inprogress', '2020-06-05 14:11:46', '2020-06-05 19:41:46', 4, 1, '', '2020-06-05 19:41:46', 'pending', 1, 'aaad3532-77b5-4c52-872b-4cbef5575db5');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `phonedetails`
--

CREATE TABLE `phonedetails` (
  `id` int(11) NOT NULL,
  `contactNo` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `productimage`
--

CREATE TABLE `productimage` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `productID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `productTitle` text NOT NULL,
  `MRP` int(10) NOT NULL,
  `productDesc` text NOT NULL,
  `imageURL` varchar(1000) NOT NULL DEFAULT 'https://lorempixel.com/700/500/technics/',
  `categoryID` int(11) NOT NULL,
  `rating` decimal(10,2) NOT NULL,
  `qtySold` int(10) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `GTIN` varchar(14) NOT NULL,
  `numberOfComment` text NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `brand` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `createdAt`, `productTitle`, `MRP`, `productDesc`, `imageURL`, `categoryID`, `rating`, `qtySold`, `weight`, `GTIN`, `numberOfComment`, `uuid`, `brand`) VALUES
(1, '2020-05-28 18:30:00', 'asd123', 100, 'asd asd', 'https://lorempixel.com/700/500/technics/', 1, '0.00', 29, '150.000', 'vzvxaczdfs6456', '', 'f5664f47-6303-4acf-a065-382ae638e38b', NULL),
(3, '2020-06-05 14:20:45', 'product 2', 1000, 'asd asd', 'https://lorempixel.com/700/500/technics/', 1, '0.00', 0, '150.000', 'vzvxaczdfsaa64', '', '72b9a03c-1d40-4cc4-8d49-203386d16bd0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fName` varchar(20) NOT NULL,
  `lName` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `gender` enum('Male','Female','Other') NOT NULL,
  `salutation` enum('Mr','Ms','Mrs','Dr') NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT current_timestamp(),
  `uuid` varchar(50) NOT NULL,
  `type` enum('customer','vendor') NOT NULL DEFAULT 'customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fName`, `lName`, `email`, `password`, `DOB`, `gender`, `salutation`, `createdAt`, `updatedAt`, `uuid`, `type`) VALUES
(3, 'admin vendor', 'user', 'adminV@asd.com', '$2b$10$14p7liF0Ssr4hIyaBF0SeOFe1kmSIGloegAI/wIt9HF7OKrxgYK9a', '2020-01-01', 'Male', 'Mr', '2020-05-28 21:56:34', '2020-05-29 03:26:34', '749f79ec-69d5-406a-b3ed-03430bae82a6', 'vendor'),
(4, 'admin customer', 'user', 'admin@asd.com', '$2b$10$K74dLw4yk168qBKKZQdxGOFr5OQY4Ng06PjCLJ2mvKuGO65zoM6xO', '2020-01-01', 'Male', 'Mr', '2020-05-28 21:56:47', '2020-05-29 03:26:47', '2ba08254-4ac9-4467-9255-06b212c174fd', 'customer'),
(5, 'vendor2', 'user', 'vendor2@asd.com', '$2b$10$zw.eKZwSFUk6FDMyY333Qu/wfvh.l1GNHdC0AV8x1cUeM7bk0RjFW', '2020-01-01', 'Male', 'Mr', '2020-06-05 14:21:43', '2020-06-05 19:51:43', 'a80b705c-60fc-462e-96ad-b7fbc00b8958', 'vendor'),
(6, 'cust2', 'asd', 'cust2@asd.com', '$2b$10$KHrrwMLlqI9waoZkoxqQjepIDgLKp.fuh6JmKo1tNxgujYtmQUvsi', '2020-06-01', 'Male', 'Mr', '2020-06-06 10:36:07', '2020-06-06 16:06:07', 'ccaef8f5-6130-4912-8b0f-5d5fdddfbf52', 'customer'),
(7, 'vendor5', 'asd', 'vendor5@asd.com', '$2b$10$bSlbwJEfryTXZtF1mXIbd..nW7lmIDRyemJjaDKLG85tHvQSOmava', '2020-06-01', 'Male', 'Mr', '2020-06-06 10:51:25', '2020-06-06 16:21:25', 'b5bf22d0-ed96-4032-855f-6618796f59d2', 'vendor');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `vendorID` int(11) NOT NULL,
  `price` int(10) NOT NULL,
  `discount` int(5) NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL,
  `categoryFilterID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`id`, `productID`, `vendorID`, `price`, `discount`, `stock`, `categoryFilterID`) VALUES
(1, 1, 3, 80, 10, 3, 1),
(4, 1, 3, 181, 2, 11, 2),
(5, 1, 3, 100, 2, 20, 3),
(6, 1, 5, 81, 2, 31, 2),
(7, 1, 5, 81, 20, 100, 3),
(9, 3, 5, 81, 20, 100, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Customer_id` (`userID`);

--
-- Indexes for table `carddetails`
--
ALTER TABLE `carddetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UserID` (`userID`);

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cartID` (`cartID`),
  ADD KEY `vendorID` (`vendorID`),
  ADD KEY `stockID` (`stockID`) USING BTREE;

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoryfilter`
--
ALTER TABLE `categoryfilter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`productID`),
  ADD KEY `customer_id` (`userID`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`orderID`,`stockID`),
  ADD KEY `order_product_ibfk_2` (`stockID`),
  ADD KEY `vendorID` (`vendorID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`userID`),
  ADD KEY `cardID` (`cardID`),
  ADD KEY `addressID` (`addressID`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`orderID`);

--
-- Indexes for table `phonedetails`
--
ALTER TABLE `phonedetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Customer_id` (`userID`);

--
-- Indexes for table `productimage`
--
ALTER TABLE `productimage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`categoryID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productID`),
  ADD KEY `vendorID` (`vendorID`),
  ADD KEY `categoryID` (`categoryFilterID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `carddetails`
--
ALTER TABLE `carddetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categoryfilter`
--
ALTER TABLE `categoryfilter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phonedetails`
--
ALTER TABLE `phonedetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productimage`
--
ALTER TABLE `productimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `carddetails`
--
ALTER TABLE `carddetails`
  ADD CONSTRAINT `carddetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`cartID`) REFERENCES `carts` (`id`),
  ADD CONSTRAINT `cartitems_ibfk_2` FOREIGN KEY (`stockID`) REFERENCES `warehouse` (`id`),
  ADD CONSTRAINT `cartitems_ibfk_3` FOREIGN KEY (`vendorID`) REFERENCES `users` (`id`);

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `categoryfilter`
--
ALTER TABLE `categoryfilter`
  ADD CONSTRAINT `categoryfilter_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `categories` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`stockID`) REFERENCES `warehouse` (`id`),
  ADD CONSTRAINT `orderitems_ibfk_3` FOREIGN KEY (`vendorID`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`cardID`) REFERENCES `carddetails` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`addressID`) REFERENCES `addresses` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `orders` (`id`);

--
-- Constraints for table `phonedetails`
--
ALTER TABLE `phonedetails`
  ADD CONSTRAINT `phonedetails_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Constraints for table `productimage`
--
ALTER TABLE `productimage`
  ADD CONSTRAINT `productimage_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `products` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `categories` (`id`);

--
-- Constraints for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `warehouse_ibfk_2` FOREIGN KEY (`vendorID`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `warehouse_ibfk_3` FOREIGN KEY (`categoryFilterID`) REFERENCES `categoryfilter` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
