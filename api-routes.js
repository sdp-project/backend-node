const router = require("express").Router();
const loginCon = require("./services/login");
const JWT = require("./middleware/verifyJWT");

const validator = require("./validators/validator");
const bodyParser = require("body-parser");
router.use(bodyParser.urlencoded({ extended: true }));

// Set default API response
router.get("/", JWT.checkToken, function (req, res) {
  res.json({
    status: "API Its Working",
    message: "Welcome to a world without rules!",
  });
});
// Export API routes

router.post("/login", validator.user.login, loginCon.login);

const userController = require("./controllers/userController");
const productController = require("./controllers/productController");
const categoryController = require("./controllers/categoryController");
const filterController = require("./controllers/filterController");
const warehouseController = require("./controllers/warehouseController");
const cartController = require("./controllers/cartController");
const addressController = require("./controllers/addressController");
const cardController = require("./controllers/cardController");
const orderController = require("./controllers/orderController");
// User routes
// Public routes
router.route("/user").post(userController.new);

router
  .route("/product")
  .get(validator.product.view, productController.index)
  .post(validator.product.create, productController.new);
router
  .route("/product/:product_id")
  .get(validator.product.search, productController.view)
  .patch(productController.update)
  .put(productController.update)
  .delete(productController.delete);

router
  .route("/category")
  .get(categoryController.index)
  .post(categoryController.new);
router
  .route("/category/:category_id")
  .get(categoryController.view)
  .patch(categoryController.update)
  .put(categoryController.update)
  .delete(categoryController.delete);

router.route("/filter").get(filterController.index).post(filterController.new);
router
  .route("/filter/:filter_id")
  .get(filterController.view)
  .patch(filterController.update)
  .put(filterController.update)
  .delete(filterController.delete);

router
  .route("/stock")
  .get(warehouseController.index)
  .post(warehouseController.new);
router
  .route("/stock/:stock_id")
  .get(warehouseController.view)
  .patch(warehouseController.update)
  .put(warehouseController.update)
  .delete(warehouseController.delete);

router
  .route("/card/:user_id")
  .get(cardController.index)
  .post(cardController.new);
router
  .route("/card/:user_id/:card_id")
  .get(cardController.view)
  .patch(cardController.update)
  .put(cardController.update)
  .delete(cardController.delete);

router
  .route("/order/:user_id")
  .get(orderController.index)
  .post(orderController.new);
router
  .route("/order/:user_id/:order_id")
  .get(orderController.view)
  .patch(orderController.update)
  .put(orderController.update)
  .delete(orderController.delete);

router.route("/cart/:user_id").get(cartController.view);
router.route("/cart/:cart_item_id/:qty").put(cartController.update);
router.route("/cart/:cart_item_id").delete(cartController.delete);
router.route("/add-to-cart").post(cartController.new);

router.use(JWT.checkToken);

// Private routes
router.route("/user").get(userController.index);
router
  .route("/user/:user_id")
  .get(userController.view)
  .patch(userController.update)
  .put(userController.update)
  .delete(userController.delete);

router
  .route("/address/:user_id")
  .get(addressController.index)
  .post(addressController.new);
router
  .route("/address/:user_id/:address_id")
  .get(addressController.view)
  .patch(addressController.update)
  .put(addressController.update)
  .delete(addressController.delete);

module.exports = router;
