const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");
const bcrypt = require("bcrypt");
const sqlQuery = require("../config/sqlQuery");

const { validationResult } = require("express-validator");

exports.login = function (req, res) {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  // For the given email fetch user from DB
  // const mockedEmail = "admin@asd.com";
  // const mockedPassword = "asd123";
  if (email && password) {
    sqlQuery("SELECT * from users where ?", { email: email }, function (
      err,
      rows
    ) {
      if (rows.length) {
        // console.log(rows[0])
        bcrypt.compare(password, rows[0].password, function (err, result) {
          if (result) {
            let token = jwt.sign({ email: email, id:rows[0].id }, jwtConfig.secret, {
              // reduce time in production to 15 min
              expiresIn: "24h", // expires in 24 hours
            });
            // return the JWT token for the future API calls
            res.json({
              success: true,
              message: "Authentication successful!",
              token: token,
            });
          } else {
            sendInvalidLogin(res);
          }
        });
      } else {
        sendInvalidLogin(res);
      }
    });
  } else {
    res.status(400);
    res.json({
      success: false,
      message: "Authentication failed! Please check the request",
    });
  }
};

const sendInvalidLogin = function (res) {
  res.status(401);
  res.json({
    success: false,
    message: "Incorrect email or password",
  });
};
