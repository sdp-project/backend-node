# sdp-backend-node

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)

## About <a name = "about"></a>

The project is done for SDP practicals.
Its done in node.js and express.js. The database is in SQL

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

To run the application you need

```
nodejs
npm
```

### Installing

Install all the dependencies from package.json. To install run the command

```
npm install
```

After successfully installing, there should be a folder called node_modules.
To run the application give the command

```
npm start
```

If everything is proper, there should be a statement showing 'Running server on port 8080'. This shows that server is up and running. Access it from [http://localhost:8080/](http://localhost:8080/). You should be able to see a welcome message. Another url is also should be available [http://localhost:8080/quotes](http://localhost:8080/quotes).

[http://localhost:8080/v1](http://localhost:8080/v1) should show success false if no auth token is given.
