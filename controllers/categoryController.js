const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  sqlQuery("SELECT * from categories", function (err, rows) {
    res.json({
      status: "success",
      message: "Categories retrieved successfully",
      data: rows,
    });
  });
};
// Handle create category actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let category = { ...req.body };
  // debugger;
  sqlQuery("INSERT INTO categories SET ?", category, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "New category created!",
        id: rows.insertId,
        category: category.catTitle,
      });
    }
  });
};
// Handle view category info
exports.view = function (req, res) {
  sqlQuery(
    "SELECT * from categories where ?",
    { id: req.params.category_id },
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Category details";
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update category info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let categoryId = req.params.category_id;
  let category = { ...req.body };
  // debugger;
  sqlQuery(
    "UPDATE categories SET ? WHERE id=" + categoryId,
    category,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: category.catTitle + " category updated!",
          category: category.catTitle,
        });
      }
    }
  );
};
// Handle delete category
exports.delete = function (req, res) {
  let categoryId = req.params.category_id;
  sqlQuery("DELETE FROM categories WHERE id=" + categoryId, function (
    err,
    rows
  ) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: categoryId + " category deleted!",
      });
    }
  });
};
