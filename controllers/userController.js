const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const bcrypt = require("bcrypt");
const saltRounds = require('../env.'+(process.env.NODE_ENV || 'development')+'.json').HASH_SALT_ROUND;
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  sqlQuery("SELECT * from users", function (err, rows) {
    console.log(rows);
    res.json({
      status: "success",
      message: "Users retrieved successfully",
      data: rows,
    });
  });
};

// Handle create user actions
exports.new = function (req, res) {
  let user = { ...req.body };
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  // user.firstName = req.body.fName;
  // user.lastName = req.body.lName;
  // user.gender = req.body.gender;
  // user.email = req.body.email;
  // user.gender = req.body.gender;
  // user.salutation = req.body.salutation;
  // user.DOB = req.body.DOB;
  // user.password = req.body.password;
  // user.type = req.body.type;
  user.uuid = uuidv4();
  // console.log(user);
  bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
    console.log("hash:" + hash);
    user.password = hash;
    // debugger;
    sqlQuery("INSERT INTO users SET ?", user, function (err, rows) {
      if (err) {
        console.log(err.message);
        const msg =
          err.code === "ER_DUP_ENTRY"
            ? "Email in use. Please use new email or use forget password"
            : "";
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: msg });
      } else {
        res.json({
          status: "success",
          message: "New user created!",
          id: user.uuid,
          username: user.email,
        });
      }
    });
    
  });
};
// Handle view user info
exports.view = function (req, res) {
  console.log(req.params.user_id);
  sqlQuery("SELECT * from users where ?", { id: req.params.user_id }, function (
    err,
    rows
  ) {
    console.log(rows);
    res.json({
      message: "User details",
      data: rows[0],
    });
  });
};
// Handle update user info
exports.update = function (req, res) {
  let user = {};
  let userId = req.params.user_id;
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.gender = req.body.gender;
  user.email = req.body.email;
  res.json({
    message: "User Info updated for " + userId,
    data: user,
  });
};
// Handle delete user
exports.delete = function (req, res) {
  res.json({
    status: "success",
    message: "User deleted",
  });
};
