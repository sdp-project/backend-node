const sqlQuery = require("../config/sqlQuery");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  sqlQuery("SELECT * from categoryfilter", function (err, rows) {
    res.json({
      status: "success",
      message: "Filters retrieved successfully",
      data: rows,
    });
  });
};
// Handle create filter actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let filter = { ...req.body };
  // debugger;
  sqlQuery("INSERT INTO categoryfilter SET ?", filter, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "New filter created for category " + filter.categoryID,
        id: rows.insertId,
      });
    }
  });
};
// Handle view filter info
exports.view = function (req, res) {
  let id = req.params.filter_id;
  sqlQuery(
    "SELECT * from categoryfilter where ?",
    { id: id },
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Category details";
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update filter info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.filter_id;
  let filter = { ...req.body };
  // debugger;
  sqlQuery(
    "UPDATE categoryfilter SET ? WHERE id=" + id,
    filter,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: "Filter updated!"
        });
      }
    }
  );
};
// Handle delete filter
exports.delete = function (req, res) {
  let id = req.params.filter_id;
  sqlQuery("DELETE FROM categoryfilter WHERE id=" + id, function (
    err,
    rows
  ) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: id + " filter deleted!",
      });
    }
  });
};
