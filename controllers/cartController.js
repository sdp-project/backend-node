const sqlQuery = require("../config/sqlQuery");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  sqlQuery("SELECT * from carts", function (err, rows) {
    res.json({
      status: "success",
      message: "Filters retrieved successfully",
      data: rows,
    });
  });
};
// Handle create filter actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let { userID, stockID, vendorID, quantity } = { ...req.body };
  // debugger;
  console.log(userID, stockID, vendorID, quantity);
  sqlQuery(
    // "CALL `addToCart`(@p0, 4, @p2, 1, 3, 6, @p6);",
    `SET @p1='${userID}'; SET @p3='${stockID}'; SET @p4='${vendorID}'; SET @p5='${quantity}'; CALL addToCart(@p0, @p1, @p2, @p3, @p4, @p5, @p6);SELECT @p0 AS OcartStatus, @p2 AS OcartID, @p6 AS OitemStatus`,
    function (err, rows) {
      debugger;
      console.log(rows[rows.length - 1]);
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: "Item added to cart",
          cartStatus: rows[rows.length - 1],
        });
      }
    }
  );
};
// Handle view filter info
exports.view = function (req, res) {
  let id = req.params.user_id;
  sqlQuery(
    "CALL `updateAndViewCartPrice`(" + id + ");",
    // "SET @p0='"+id+"'; CALL `updateAndViewCartPrice`(@p0);",
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Cart list";
      console.log(rows);
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update filter info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.cart_item_id;
  let qty = req.params.qty;
  if (qty == 0) {
    deleteCartItem(id, req, res);
    return;
  }
  console.log("updTE CART", id, qty);
  // debugger;
  sqlQuery(
    "UPDATE cartitems SET ? WHERE id=" + id,
    { quantity: qty },
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: "Cart updated!",
        });
      }
    }
  );
};
// Handle delete filter

exports.delete = function (req, res) {
  let id = req.params.cart_item_id;

  deleteCartItem(id, req, res);
};

function deleteCartItem(id, req, res) {
  console.log("deleteing");
  sqlQuery("DELETE FROM cartitems WHERE id=" + id, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: id + " item deleted!",
      });
    }
  });
}
