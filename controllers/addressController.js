const sqlQuery = require("../config/sqlQuery");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  let userID = req.params.user_id;
  sqlQuery(
    "SELECT id, address1, address2, city, state, postalCode, country, mobileNumber, type from addresses WHERE ?",
    { userID: userID },
    function (err, rows) {
      res.json({
        status: "success",
        message: "addresses retrieved successfully",
        data: rows,
      });
    }
  );
};
// Handle create addresses actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let addresses = { ...req.body };
  // debugger;
  sqlQuery("INSERT INTO addresses SET ?", addresses, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "New addresses created!",
        id: rows.insertId,
        addresses: addresses.addresses,
      });
    }
  });
};
// Handle view addresses info
exports.view = function (req, res) {
  let userID = req.params.user_id;
  let id = req.params.address_id;
  sqlQuery(
    `SELECT address1, address2, city, state, postalCode, country, mobileNumber, type from addresses WHERE id=${id} AND userID = ${userID}`,
    function (err, rows) {
      const msg = rows.length == 0 ? "No address found" : "Address details";
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update addresses info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.address_id;
  let addresses = { ...req.body };
  // debugger;
  sqlQuery("UPDATE addresses SET ? WHERE id=" + id, addresses, function (
    err,
    rows
  ) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: id + ": address updated!",
      });
    }
  });
};
// Handle delete addresses
exports.delete = function (req, res) {
  let id = req.params.address_id;
  let userID = req.params.user_id;
  sqlQuery(
    `DELETE FROM addresses WHERE id=${id} AND userID = ${userID}`,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: id + ": addresses deleted!",
        });
      }
    }
  );
};
