const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  sqlQuery("SELECT * from warehouse", function (err, rows) {
    res.json({
      status: "success",
      message: "Stocks retrieved successfully",
      data: rows,
    });
  });
};
// Handle create warehouse actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let warehouseItem = { ...req.body };
  // debugger;
  sqlQuery("INSERT INTO warehouse SET ?", warehouseItem, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "Stock added!",
        id: rows.insertId,
      });
    }
  });
};
// Handle view warehouse info
exports.view = function (req, res) {
  sqlQuery(
    "SELECT * from warehouse where ?",
    { id: req.params.stock_id },
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Stock details";
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update warehouseItem info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let stockID = req.params.stock_id;
  let warehouseItem = { ...req.body };
  // debugger;
  sqlQuery(
    "UPDATE warehouse SET ? WHERE id=" + stockID,
    warehouseItem,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: " stock updated!",
        });
      }
    }
  );
};
// Handle delete warehouse
exports.delete = function (req, res) {
  let stockID = req.params.stock_id;
  sqlQuery("DELETE FROM warehouse WHERE id=" + stockID, function (
    err,
    rows
  ) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: stockID + " stock deleted!",
      });
    }
  });
};
