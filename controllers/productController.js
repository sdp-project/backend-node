const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let searchParam = { ...req.query };
  console.log(searchParam);
  searchParam.name = searchParam.name ? searchParam.name : "";
  searchParam.order = searchParam.order ? searchParam.order : "";
  searchParam.filter = searchParam.filter ? searchParam.filter : "MRP";
  searchParam.start = searchParam.start ? searchParam.start : "0";
  searchParam.end = searchParam.end ? searchParam.end : "10";
  let categoryQuery = '';
  console.log(searchParam.categoryID);
  if (searchParam.categoryID) {
    categoryQuery = `categoryID=${searchParam.categoryID} AND `;
  }
  sqlQuery(
    `SELECT * FROM products WHERE ${categoryQuery}productTitle LIKE '%${searchParam.name}%' ORDER BY ${searchParam.filter} ${searchParam.order} LIMIT ${searchParam.start},${searchParam.end}`,
    function (err, rows) {
      // console.log(rows);
      res.json({
        status: "success",
        message: "Products retrieved successfully",
        count: rows.length,
        data: rows,
      });
    }
  );
};
// Handle create product actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let product = { ...req.body };
  product.uuid = uuidv4();
  console.log(product);
  // debugger;
  sqlQuery("INSERT INTO products SET ?", product, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "New product created!",
        id: product.uuid,
        product: product.name,
      });
    }
  });
};
// Handle view product info
exports.view = function (req, res) {
  console.log(req.params.product_id);
  sqlQuery(
    `SET @p0='${req.params.product_id}'; CALL viewProductAndVendors(@p0);`,
    // "SELECT * from products where ?",
    // { uuid: req.params.product_id },
    function (err, rows) {
      // console.log(rows);
      const msg = rows.length == 0 ? "No data found" : "Product details";
      res.json({
        message: msg,
        product: rows[1],
        vendorList: rows[2],
      });
    }
  );
};
// Handle update product info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.product_id;
  let productDetails = { ...req.body };
  // debugger;
  sqlQuery("UPDATE products SET ? WHERE id=" + id, productDetails, function (
    err,
    rows
  ) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        message: "Product Info updated for " + id,
        status: "success",
      });
    }
  });
};
// Handle delete product
exports.delete = function (req, res) {
  res.json({
    status: "success",
    message: "Product deleted",
  });
};
