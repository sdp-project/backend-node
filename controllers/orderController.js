const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  let userID = req.params.user_id;
  sqlQuery("SELECT * FROM orders WHERE ?", { userID: userID }, function (
    err,
    rows
  ) {
    res.json({
      status: "success",
      message: "order retrieved successfully",
      data: rows,
    });
  });
};
// Handle create orders actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let orders = { ...req.body };
  orders.uuid = uuidv4();
  // debugger;
  sqlQuery(
    `SET @p0=${orders.userID}; SET @p1=${orders.cardID}; SET @p2=${orders.addressID}; SET @p3="${orders.uuid}"; CALL placeOrder(@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7); SELECT @p4 AS Oflag, @p5 AS OorderI, @p6 AS Omsg, @p7 AS OcartCount;`,
    orders,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        console.log(rows[rows.length - 1]);
        const data = rows[rows.length - 1];
        res.json({
          status: data[0].Oflag == 1 ? "success" : "error",
          message: data[0].Omsg,
          id: orders.uuid,
        });
      }
    }
  );
};
// Handle view orders info
exports.view = function (req, res) {
  let userID = req.params.user_id;
  let id = req.params.order_id;
  sqlQuery(
    `SELECT * FROM orderitems WHERE orderID=${id}`,
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Order details";
      // console.log(rows)
      res.json({
        message: msg,
        data: rows,
      });
    }
  );
};
// Handle update orders info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.order_id;
  let orders = { ...req.body };
  // debugger;
  sqlQuery("UPDATE orders SET ? WHERE id=" + id, orders, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "order details updated!",
      });
    }
  });
};
// Handle delete orders
exports.delete = function (req, res) {
  let id = req.params.order_id;
  sqlQuery("DELETE FROM orders WHERE id=" + id, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: id + " orders deleted!",
      });
    }
  });
};
