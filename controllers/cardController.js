const sqlQuery = require("../config/sqlQuery");
const { v4: uuidv4 } = require("uuid");
const { validationResult } = require("express-validator");
// Handle index actions
exports.index = function (req, res) {
  let userID = req.params.user_id;
  sqlQuery(
    "SELECT id, RIGHT(cardNumber,4) as cardNumber, nameOnCard, type, provider, expiryMM, expiryYY FROM carddetails WHERE ?",
    { userID: userID },
    function (err, rows) {
      res.json({
        status: "success",
        message: "card retrieved successfully",
        data: rows,
      });
    }
  );
};
// Handle create carddetails actions
exports.new = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let carddetails = { ...req.body };
  carddetails.uuid = uuidv4();
  // debugger;
  sqlQuery("INSERT INTO carddetails SET ?", carddetails, function (err, rows) {
    if (err) {
      console.log(err.message);
      return res
        .status(400)
        .send({ status: "failed", code: err.code, message: err.message });
    } else {
      res.json({
        status: "success",
        message: "New card added!",
        id: rows.insertId,
      });
    }
  });
};
// Handle view carddetails info
exports.view = function (req, res) {
  let userID = req.params.user_id;
  let id = req.params.card_id;
  sqlQuery(
    `SELECT RIGHT(cardNumber,4) as cardNumber, nameOnCard, type, provider, expiryMM, expiryYY FROM carddetails WHERE id=${id} AND userID = ${userID}`,
    function (err, rows) {
      const msg = rows.length == 0 ? "No data found" : "Card details";
      res.json({
        message: msg,
        data: rows[0],
      });
    }
  );
};
// Handle update carddetails info
exports.update = function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let id = req.params.card_id;
  let carddetails = { ...req.body };
  // debugger;
  sqlQuery(
    "UPDATE carddetails SET ? WHERE id=" + id,
    carddetails,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: "card details updated!",
        });
      }
    }
  );
};
// Handle delete carddetails
exports.delete = function (req, res) {
  let id = req.params.card_id;
  sqlQuery(
    "DELETE FROM carddetails WHERE id=" + id,
    function (err, rows) {
      if (err) {
        console.log(err.message);
        return res
          .status(400)
          .send({ status: "failed", code: err.code, message: err.message });
      } else {
        res.json({
          status: "success",
          message: id + " carddetails deleted!",
        });
      }
    }
  );
};
