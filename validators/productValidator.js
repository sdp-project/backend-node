const { check, body, query } = require("express-validator");

exports.create = [
  check("productTitle").notEmpty(),
  check("MRP").notEmpty(),
  check("GTIN").notEmpty(),
  check("categoryID").notEmpty(),
];

exports.search = [check("id").isUUID()];

exports.view = [
  query("name").trim().blacklist("\\<\\>\\;\\*\\%\\$\\#\\^\\@\\'\\\"\\^!?"),
];
