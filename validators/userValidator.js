const { check, body } = require("express-validator");

exports.login = [
  check("email").isEmail(),
  body("email").isEmail().normalizeEmail(),
];

exports.register = [
  check("fName").notEmpty(),
  check("lName").notEmpty(),
  check("gender").notEmpty(),
  check("salutation").notEmpty(),
  check("DOB").notEmpty(),
  check("type").notEmpty(),
  check("email").isEmail(),
  check("password").isLength({ min: 8 }),
];
