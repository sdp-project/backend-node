// Import express
const express = require("express");
// Initialize the app
const app = express();
const cors = require('cors')
const bodyParser = require("body-parser");
// Setup server port
const port = process.env.PORT || 8080;
// Launch app to listen to specified port
app.listen(port, function () {
  console.log((process.env.NODE_ENV || 'development')+" server on port " + port);
});

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
// Send message for default URL
app.get("/", (req, res) => res.send("Welcome to the desert of the real. - The Matrix"));
app.get("/quotes", (req, res) => {
  res.json({
    status: "success",
    quotes: "If you are good at something never do it for free.",
  });
});

// Import routes
let apiRoutes = require("./api-routes");
// Use Api routes in the App
app.use("/v1", apiRoutes);
